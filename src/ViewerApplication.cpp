#include "ViewerApplication.hpp"

#include <algorithm>
#include <iostream>
#include <numeric>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>

#include "utils/cameras.hpp"
#include "utils/gltf.hpp"
#include "utils/images.hpp"

#include <stb_image_write.h>
#include <tiny_gltf.h>

constexpr GLuint VERTEX_ATTRIB_POSITION_IDX = 0;
constexpr GLuint VERTEX_ATTRIB_NORMAL_IDX = 1;
constexpr GLuint VERTEX_ATTRIB_TEXCOORD0_IDX = 2;

///////////////////////////////////////////////////////////////////////////////
void keyCallback(
    GLFWwindow *window, int key, int scancode, int action, int mods)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
    glfwSetWindowShouldClose(window, 1);
  }
}

///////////////////////////////////////////////////////////////////////////////
int ViewerApplication::run()
{
  // Loader shaders
  const auto glslProgram = compileProgram({m_ShadersRootPath / m_vertexShader,
      m_ShadersRootPath / m_fragmentShader});

  const auto modelViewProjMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewProjMatrix");
  const auto modelViewMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewMatrix");
  const auto normalMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uNormalMatrix");
  const auto uLightDirectionLocation =
      glGetUniformLocation(glslProgram.glId(), "uLightDirection");
  const auto uLightIntensity =
      glGetUniformLocation(glslProgram.glId(), "uLightIntensity");
  const auto uBaseColorTexture =
      glGetUniformLocation(glslProgram.glId(), "uBaseColorTexture");
  const auto uBaseColorFactor =
      glGetUniformLocation(glslProgram.glId(), "uBaseColorFactor");
  const auto uMetallicRoughnessTexture =
      glGetUniformLocation(glslProgram.glId(), "uMetallicRoughnessTexture");
  const auto uMetallicFactor =
      glGetUniformLocation(glslProgram.glId(), "uMetallicFactor");
  const auto uRoughnessFactor =
      glGetUniformLocation(glslProgram.glId(), "uRoughnessFactor");
  const auto uEmissiveTexture =
      glGetUniformLocation(glslProgram.glId(), "uEmissiveTexture");
  const auto uEmissiveFactor =
      glGetUniformLocation(glslProgram.glId(), "uEmissiveFactor");
  const auto uOcclusionTexture =
      glGetUniformLocation(glslProgram.glId(), "uOcclusionTexture");
  const auto uOcclusionStrength =
      glGetUniformLocation(glslProgram.glId(), "uOcclusionStrength");
  const auto uApplyOcclusion =
      glGetUniformLocation(glslProgram.glId(), "uApplyOcclusion");
  const auto uNormalMapTexture =
      glGetUniformLocation(glslProgram.glId(), "uNormalMapTexture");
  const auto uNormalMapScale =
      glGetUniformLocation(glslProgram.glId(), "uNormalMapScale");
  const auto uApplyNormalMapping =
      glGetUniformLocation(glslProgram.glId(), "uApplyNormalMapping");

  glm::vec3 lightDirection(1, 1, 1);
  glm::vec3 lightIntensity(1, 1, 1);
  bool lightFromCamera = false;
  bool applyOcclusion = true;
  bool applyNormalMapping = true;

  tinygltf::Model model;
  loadGltfFile(model);
  std::vector<GLuint> bufferObjects = createBufferObjects(model);
  std::vector<VaoRange> meshIndexToVaoRange;
  const auto vertexArrayObjects =
      createVertexArrayObjects(model, bufferObjects, meshIndexToVaoRange);

  const auto textureObjects = createTextureObjects(model);

  // Create white texture for object with no base color texture
  GLuint whiteTexture = 0;
  float white[] = {1, 1, 1, 1};
  glGenTextures(1, &whiteTexture);
  glBindTexture(GL_TEXTURE_2D, whiteTexture);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_FLOAT, white);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);
  glBindTexture(GL_TEXTURE_2D, 0);

  // Build projection matrix
  glm::vec3 bboxMin, bboxMax;
  computeSceneBounds(model, bboxMin, bboxMax);
  const auto diag = bboxMax - bboxMin;
  const auto maxDistance = glm::length(diag);
  const auto projMatrix =
      glm::perspective(70.f, float(m_nWindowWidth) / m_nWindowHeight,
          0.001f * maxDistance, 1.5f * maxDistance);

  m_cameraMap[CameraType::FIRST_PERSON] =
      std::make_unique<FirstPersonCameraController>(
          m_GLFWHandle.window(), 0.5f * maxDistance);
  m_cameraMap[CameraType::TRACKBALL] =
      std::make_unique<TrackballCameraController>(
          m_GLFWHandle.window(), 0.5f * maxDistance);

  if (m_hasUserCamera) {
    m_cameraMap[m_activeCameraType]->setCamera(m_userCamera);
  } else {
    // TODO Use scene bounds to compute a better default camera
    const auto center = 0.5f * (bboxMax + bboxMin);
    const auto up = glm::vec3(0, 1, 0);
    const auto eye =
        diag.z > 0 ? center + diag : center + 2.f * glm::cross(diag, up);
    m_cameraMap[m_activeCameraType]->setCamera(Camera{eye, center, up});
  }

  // Setup OpenGL state for rendering
  glEnable(GL_DEPTH_TEST);
  glslProgram.use();

  const auto bindMaterial = [&](const auto materialIndex) {
    if (materialIndex >= 0) {
      const auto &material = model.materials[materialIndex];
      const auto &pbrMetallicRoughness = material.pbrMetallicRoughness;
      if (uBaseColorFactor >= 0) {
        glUniform4f(uBaseColorFactor,
            (float)pbrMetallicRoughness.baseColorFactor[0],
            (float)pbrMetallicRoughness.baseColorFactor[1],
            (float)pbrMetallicRoughness.baseColorFactor[2],
            (float)pbrMetallicRoughness.baseColorFactor[3]);
      }
      if (uBaseColorTexture >= 0) {
        auto textureObject = whiteTexture;
        if (pbrMetallicRoughness.baseColorTexture.index >= 0) {
          const auto &texture =
              model.textures[pbrMetallicRoughness.baseColorTexture.index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(uBaseColorTexture, 0);
      }
      if (uMetallicFactor >= 0) {
        glUniform1f(
            uMetallicFactor, (float)pbrMetallicRoughness.metallicFactor);
      }
      if (uRoughnessFactor >= 0) {
        glUniform1f(
            uRoughnessFactor, (float)pbrMetallicRoughness.roughnessFactor);
      }
      if (uMetallicRoughnessTexture >= 0) {
        auto textureObject = 0u;
        if (pbrMetallicRoughness.metallicRoughnessTexture.index >= 0) {
          const auto &texture =
              model.textures[pbrMetallicRoughness.metallicRoughnessTexture
                                 .index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(uMetallicRoughnessTexture, 1);
      }
      if (uEmissiveFactor >= 0) {
        glUniform3f(uEmissiveFactor, (float)material.emissiveFactor[0],
            (float)material.emissiveFactor[1],
            (float)material.emissiveFactor[2]);
      }
      if (uEmissiveTexture >= 0) {
        auto textureObject = 0u;
        if (material.emissiveTexture.index >= 0) {
          const auto &texture = model.textures[material.emissiveTexture.index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(uEmissiveTexture, 2);
      }
      if (uOcclusionStrength >= 0) {
        glUniform1f(
            uOcclusionStrength, (float)material.occlusionTexture.strength);
      }
      if (uOcclusionTexture >= 0) {
        auto textureObject = whiteTexture;
        if (material.occlusionTexture.index >= 0) {
          const auto &texture = model.textures[material.occlusionTexture.index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(uOcclusionTexture, 3);
      }
      if (uApplyOcclusion >= 0) {
        glUniform1i(uApplyOcclusion, applyOcclusion);
      }
      if (uNormalMapTexture >= 0) {
        auto textureObject = whiteTexture;
        if (material.normalTexture.index >= 0) {
          const auto &texture = model.textures[material.normalTexture.index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE4);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(uNormalMapTexture, 4);
      }
      if (uNormalMapScale >= 0) {
        glUniform1f(uNormalMapScale, (float)material.normalTexture.scale);
      }
      if (uApplyOcclusion >= 0) {
        glUniform1i(uApplyNormalMapping, applyNormalMapping);
      }
    } else {
      // Apply default material
      // Defined here:
      // https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#reference-material
      // https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#reference-pbrmetallicroughness3
      if (uBaseColorFactor >= 0) {
        glUniform4f(uBaseColorFactor, 1, 1, 1, 1);
      }
      if (uBaseColorTexture >= 0) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, whiteTexture);
        glUniform1i(uBaseColorTexture, 0);
      }
      if (uMetallicFactor >= 0) {
        glUniform1f(uMetallicFactor, 1.f);
      }
      if (uRoughnessFactor >= 0) {
        glUniform1f(uRoughnessFactor, 1.f);
      }
      if (uMetallicRoughnessTexture >= 0) {
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(uMetallicRoughnessTexture, 1);
      }
      if (uEmissiveFactor >= 0) {
        glUniform3f(uEmissiveFactor, 0.f, 0.f, 0.f);
      }
      if (uEmissiveTexture >= 0) {
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(uEmissiveTexture, 2);
      }
      if (uOcclusionStrength >= 0) {
        glUniform1f(uOcclusionStrength, 0.f);
      }
      if (uOcclusionTexture >= 0) {
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(uOcclusionTexture, 3);
      }
      if (uApplyOcclusion >= 0) {
        glUniform1i(uApplyOcclusion, applyOcclusion);
      }
      if (uNormalMapTexture >= 0) {
        glActiveTexture(GL_TEXTURE4);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(uNormalMapTexture, 4);
      }
      if (uNormalMapScale >= 0) {
        glUniform1f(uNormalMapScale, 0.f);
      }
      if (uApplyOcclusion >= 0) {
        glUniform1i(uApplyNormalMapping, applyNormalMapping);
      }
    }
  };

  // Lambda function to draw the scene
  const auto drawScene = [&](const Camera &camera) {
    glViewport(0, 0, m_nWindowWidth, m_nWindowHeight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    const auto viewMatrix = camera.getViewMatrix();

    if (uLightDirectionLocation >= 0) {
      if (lightFromCamera) {
        glUniform3f(uLightDirectionLocation, 0, 0, 1);
      } else {
        const auto lightDirectionInViewSpace = glm::normalize(
            glm::vec3(viewMatrix * glm::vec4(lightDirection, 0.)));
        glUniform3f(uLightDirectionLocation, lightDirectionInViewSpace[0],
            lightDirectionInViewSpace[1], lightDirectionInViewSpace[2]);
      }
      if (uLightIntensity >= 0) {
        glUniform3f(uLightIntensity, lightIntensity[0], lightIntensity[1],
            lightIntensity[2]);
      }
    }

    // The recursive function that should draw a node
    // We use a std::function because a simple lambda cannot be recursive
    const std::function<void(int, const glm::mat4 &)> drawNode =
        [&](int nodeIdx, const glm::mat4 &parentMatrix) {
          const auto &node = model.nodes[nodeIdx];
          const glm::mat4 modelMatrix =
              getLocalToWorldMatrix(node, parentMatrix);
          // If the node references a mesh (a node can also reference a
          // camera, or a light)
          if (node.mesh >= 0) {
            const auto mvMatrix =
                viewMatrix * modelMatrix; // Also called localToCamera matrix
            const auto mvpMatrix =
                projMatrix * mvMatrix; // Also called localToScreen matrix
            // Normal matrix is necessary to maintain normal vectors
            // orthogonal to tangent vectors
            // https://www.lighthouse3d.com/tutorials/glsl-12-tutorial/the-normal-matrix/
            const auto normalMatrix = glm::transpose(glm::inverse(mvMatrix));

            glUniformMatrix4fv(modelViewProjMatrixLocation, 1, GL_FALSE,
                glm::value_ptr(mvpMatrix));
            glUniformMatrix4fv(
                modelViewMatrixLocation, 1, GL_FALSE, glm::value_ptr(mvMatrix));
            glUniformMatrix4fv(normalMatrixLocation, 1, GL_FALSE,
                glm::value_ptr(normalMatrix));

            const auto &mesh = model.meshes[node.mesh];
            const auto &vaoRange = meshIndexToVaoRange[node.mesh];
            for (size_t pIdx = 0; pIdx < mesh.primitives.size(); ++pIdx) {
              const auto vao = vertexArrayObjects[vaoRange.begin + pIdx];
              const auto &primitive = mesh.primitives[pIdx];
              bindMaterial(primitive.material);
              glBindVertexArray(vao);
              if (primitive.indices >= 0) {
                const auto &accessor = model.accessors[primitive.indices];
                const auto &bufferView = model.bufferViews[accessor.bufferView];
                const auto byteOffset =
                    accessor.byteOffset + bufferView.byteOffset;
                glDrawElements(primitive.mode, GLsizei(accessor.count),
                    accessor.componentType, (const GLvoid *)byteOffset);
              } else {
                // Take first accessor to get the count
                const auto accessorIdx = (*begin(primitive.attributes)).second;
                const auto &accessor = model.accessors[accessorIdx];
                glDrawArrays(primitive.mode, 0, GLsizei(accessor.count));
              }
            }
          }

          for (const auto childNodeIdx : node.children) {
            drawNode(childNodeIdx, modelMatrix);
          }
        };

    // Draw the scene referenced by gltf file
    if (model.defaultScene >= 0) {
      for (auto &nodeIdx : model.scenes[model.defaultScene].nodes) {
        drawNode(nodeIdx, glm::mat4(1));
      }
    }
  };

  if (!m_OutputPath.empty()) {
    const auto numComponents = 3;
    std::vector<unsigned char> pixels(
        m_nWindowWidth * m_nWindowHeight * numComponents);
    renderToImage(
        m_nWindowWidth, m_nWindowHeight, numComponents, pixels.data(), [&]() {
          const auto camera = m_cameraMap[m_activeCameraType]->getCamera();
          drawScene(camera);
        });
    flipImageYAxis(
        m_nWindowWidth, m_nWindowHeight, numComponents, pixels.data());
    const auto strPath = m_OutputPath.string();
    stbi_write_png(
        strPath.c_str(), m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), 0);
    return 0;
  }

  // Loop until the user closes the window
  for (auto iterationCount = 0u; !m_GLFWHandle.shouldClose();
       ++iterationCount) {
    const auto seconds = glfwGetTime();

    const auto camera = m_cameraMap[m_activeCameraType]->getCamera();
    drawScene(camera);

    // GUI code:
    imguiNewFrame();

    {
      ImGui::Begin("GUI");
      ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
          1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
      if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::Text("eye: %.3f %.3f %.3f", camera.eye().x, camera.eye().y,
            camera.eye().z);
        ImGui::Text("center: %.3f %.3f %.3f", camera.center().x,
            camera.center().y, camera.center().z);
        ImGui::Text(
            "up: %.3f %.3f %.3f", camera.up().x, camera.up().y, camera.up().z);

        ImGui::Text("front: %.3f %.3f %.3f", camera.front().x, camera.front().y,
            camera.front().z);
        ImGui::Text("left: %.3f %.3f %.3f", camera.left().x, camera.left().y,
            camera.left().z);

        if (ImGui::Button("CLI camera args to clipboard")) {
          std::stringstream ss;
          ss << "--lookat " << camera.eye().x << "," << camera.eye().y << ","
             << camera.eye().z << "," << camera.center().x << ","
             << camera.center().y << "," << camera.center().z << ","
             << camera.up().x << "," << camera.up().y << "," << camera.up().z;
          const auto str = ss.str();
          glfwSetClipboardString(m_GLFWHandle.window(), str.c_str());
        }

        static int selectedCameraType = 0;
        auto cameraTypeChanged =
            ImGui::RadioButton("Trackball", &selectedCameraType, 0) ||
            ImGui::RadioButton("First Person", &selectedCameraType, 1);
        if (cameraTypeChanged) {
          auto currentCamera = m_cameraMap[m_activeCameraType]->getCamera();
          if (selectedCameraType == 0) {
            m_activeCameraType = CameraType::TRACKBALL;
          } else {
            m_activeCameraType = CameraType::FIRST_PERSON;
          }
          m_cameraMap[m_activeCameraType]->setCamera(currentCamera);
        }
      }

      if (ImGui::CollapsingHeader("Light", ImGuiTreeNodeFlags_DefaultOpen)) {
        static float lightTheta = 0.f;
        static float lightPhi = 0.f;

        if (ImGui::SliderFloat("theta", &lightTheta, 0, glm::pi<float>()) ||
            ImGui::SliderFloat("phi", &lightPhi, 0, 2.f * glm::pi<float>())) {
          const auto sinPhi = glm::sin(lightPhi);
          const auto cosPhi = glm::cos(lightPhi);
          const auto sinTheta = glm::sin(lightTheta);
          const auto cosTheta = glm::cos(lightTheta);
          lightDirection =
              glm::vec3(sinTheta * cosPhi, cosTheta, sinTheta * sinPhi);
        }

        static glm::vec3 lightColor(1.f, 1.f, 1.f);
        static float lightIntensityFactor = 1.f;

        if (ImGui::ColorEdit3("color", (float *)&lightColor) ||
            ImGui::InputFloat("intensity", &lightIntensityFactor)) {
          lightIntensity = lightColor * lightIntensityFactor;
        }
        ImGui::Checkbox("light from camera", &lightFromCamera);
        ImGui::Checkbox("apply occlusion", &applyOcclusion);
        ImGui::Checkbox("apply normal mapping", &applyNormalMapping);
      }

      ImGui::End();
    }

    imguiRenderFrame();

    glfwPollEvents(); // Poll for and process events

    auto ellapsedTime = glfwGetTime() - seconds;
    auto guiHasFocus =
        ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantCaptureKeyboard;
    if (!guiHasFocus) {
      m_cameraMap[m_activeCameraType]->update(float(ellapsedTime));
    }

    m_GLFWHandle.swapBuffers(); // Swap front and back buffers
  }

  // TODO clean up allocated GL data

  return 0;
}

///////////////////////////////////////////////////////////////////////////////
ViewerApplication::ViewerApplication(const fs::path &appPath, uint32_t width,
    uint32_t height, const fs::path &gltfFile,
    const std::vector<float> &lookatArgs, const std::string &vertexShader,
    const std::string &fragmentShader, const fs::path &output) :
    m_nWindowWidth(width),
    m_nWindowHeight(height),
    m_AppPath{appPath},
    m_AppName{m_AppPath.stem().string()},
    m_ImGuiIniFilename{m_AppName + ".imgui.ini"},
    m_ShadersRootPath{m_AppPath.parent_path() / "shaders"},
    m_gltfFilePath{gltfFile},
    m_OutputPath{output}
{
  if (!lookatArgs.empty()) {
    m_hasUserCamera = true;
    m_userCamera =
        Camera{glm::vec3(lookatArgs[0], lookatArgs[1], lookatArgs[2]),
            glm::vec3(lookatArgs[3], lookatArgs[4], lookatArgs[5]),
            glm::vec3(lookatArgs[6], lookatArgs[7], lookatArgs[8])};
  }

  if (!vertexShader.empty()) {
    m_vertexShader = vertexShader;
  }

  if (!fragmentShader.empty()) {
    m_fragmentShader = fragmentShader;
  }

  ImGui::GetIO().IniFilename =
      m_ImGuiIniFilename.c_str(); // At exit, ImGUI will store its windows
                                  // positions in this file

  glfwSetKeyCallback(m_GLFWHandle.window(), keyCallback);

  printGLVersion();
}

///////////////////////////////////////////////////////////////////////////////
bool ViewerApplication::loadGltfFile(tinygltf::Model &model)
{
  tinygltf::TinyGLTF loader;
  std::string err;
  std::string warn;
  bool ret =
      loader.LoadASCIIFromFile(&model, &err, &warn, m_gltfFilePath.string());
  if (!warn.empty()) {
    std::cerr << "Warn: " << warn.c_str() << std::endl;
  }
  if (!err.empty()) {
    std::cerr << "Err: " << err.c_str() << std::endl;
  }
  return ret;
}

///////////////////////////////////////////////////////////////////////////////
std::vector<GLuint> ViewerApplication::createBufferObjects(
    const tinygltf::Model &model)
{
  auto buffers = model.buffers;
  std::vector<GLuint> ret_bufferObjects(buffers.size());
  glGenBuffers(GLsizei(ret_bufferObjects.size()), ret_bufferObjects.data());
  for (size_t i = 0; i < ret_bufferObjects.size(); ++i) {
    glBindBuffer(GL_ARRAY_BUFFER, ret_bufferObjects[i]);
    glBufferStorage(
        GL_ARRAY_BUFFER, buffers[i].data.size(), buffers[i].data.data(), 0);
  }
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  return ret_bufferObjects;
}

///////////////////////////////////////////////////////////////////////////////
std::vector<GLuint> ViewerApplication::createVertexArrayObjects(
    const tinygltf::Model &model, const std::vector<GLuint> &bufferObjects,
    std::vector<VaoRange> &meshIndexToVaoRange)
{
  std::vector<GLuint> ret_vertexArrayObjects;
  meshIndexToVaoRange.resize(model.meshes.size());

  for (auto &mesh : model.meshes) {
    const auto vaoOffset = ret_vertexArrayObjects.size();
    const auto primitiveSize = mesh.primitives.size();

    ret_vertexArrayObjects.resize(vaoOffset + primitiveSize);
    meshIndexToVaoRange.push_back(
        VaoRange{GLsizei(vaoOffset), GLsizei(primitiveSize)});
    glGenVertexArrays(
        GLsizei(primitiveSize), &ret_vertexArrayObjects[vaoOffset]);

    for (size_t pIdx = 0; pIdx < mesh.primitives.size(); pIdx++) {
      const auto &primitive = mesh.primitives[pIdx];
      glBindVertexArray(ret_vertexArrayObjects[vaoOffset + pIdx]);

      const auto posIterator = primitive.attributes.find("POSITION");
      if (posIterator != end(primitive.attributes)) {
        const auto accessorIdx = (*posIterator).second;
        const auto &accessor = model.accessors[accessorIdx];
        const auto &bufferView = model.bufferViews[accessor.bufferView];
        const auto bufferIdx = bufferView.buffer;
        glEnableVertexAttribArray(VERTEX_ATTRIB_POSITION_IDX);
        glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);
        const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
        glVertexAttribPointer(VERTEX_ATTRIB_POSITION_IDX, accessor.type,
            accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride),
            (const GLvoid *)byteOffset);
      }

      const auto normalIterator = primitive.attributes.find("NORMAL");
      if (normalIterator != end(primitive.attributes)) {
        const auto accessorIdx = (*normalIterator).second;
        const auto &accessor = model.accessors[accessorIdx];
        const auto &bufferView = model.bufferViews[accessor.bufferView];
        const auto bufferIdx = bufferView.buffer;
        glEnableVertexAttribArray(VERTEX_ATTRIB_NORMAL_IDX);
        glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);
        const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
        glVertexAttribPointer(VERTEX_ATTRIB_NORMAL_IDX, accessor.type,
            accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride),
            (const GLvoid *)byteOffset);
      }

      const auto texIterator = primitive.attributes.find("TEXCOORD_0");
      if (texIterator != end(primitive.attributes)) {
        const auto accessorIdx = (*texIterator).second;
        const auto &accessor = model.accessors[accessorIdx];
        const auto &bufferView = model.bufferViews[accessor.bufferView];
        const auto bufferIdx = bufferView.buffer;
        glEnableVertexAttribArray(VERTEX_ATTRIB_TEXCOORD0_IDX);
        glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);
        const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
        glVertexAttribPointer(VERTEX_ATTRIB_TEXCOORD0_IDX, accessor.type,
            accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride),
            (const GLvoid *)byteOffset);
      }

      if (primitive.indices >= 0) {
        const auto accessorIdx = primitive.indices;
        const auto &accessor = model.accessors[accessorIdx];
        const auto &bufferView = model.bufferViews[accessor.bufferView];
        const auto bufferIdx = bufferView.buffer;
        assert(GL_ELEMENT_ARRAY_BUFFER == bufferView.target);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
            bufferObjects[bufferIdx]); // Binding the index buffer
                                       // to GL_ELEMENT_ARRAY_BUFFER
                                       // while the VAO is bound is
                                       // enough to tell OpenGL we
                                       // want to use that index
                                       // buffer for that VAO
      }
    }
  }

  glBindVertexArray(0);
  std::cout << "Number of VAOs: " << ret_vertexArrayObjects.size() << std::endl;
  return ret_vertexArrayObjects;
}

///////////////////////////////////////////////////////////////////////////////
std::vector<GLuint> ViewerApplication::createTextureObjects(
    const tinygltf::Model &model) const
{
  std::vector<GLuint> textureObjects(model.textures.size(), 0);

  // default sampler:
  // https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#texturesampler
  // "When undefined, a sampler with repeat wrapping and auto filtering should
  // be used."
  tinygltf::Sampler defaultSampler;
  defaultSampler.minFilter = GL_LINEAR;
  defaultSampler.magFilter = GL_LINEAR;
  defaultSampler.wrapS = GL_REPEAT;
  defaultSampler.wrapT = GL_REPEAT;
  defaultSampler.wrapR = GL_REPEAT;

  glActiveTexture(GL_TEXTURE0);

  glGenTextures(GLsizei(model.textures.size()), textureObjects.data());
  for (size_t i = 0; i < model.textures.size(); ++i) {
    const auto &texture = model.textures[i];
    assert(texture.source >= 0);
    const auto &image = model.images[texture.source];

    const auto &sampler =
        texture.sampler >= 0 ? model.samplers[texture.sampler] : defaultSampler;
    glBindTexture(GL_TEXTURE_2D, textureObjects[i]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0,
        GL_RGBA, image.pixel_type, image.image.data());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
        sampler.minFilter != -1 ? sampler.minFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
        sampler.magFilter != -1 ? sampler.magFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sampler.wrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, sampler.wrapT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, sampler.wrapR);

    if (sampler.minFilter == GL_NEAREST_MIPMAP_NEAREST ||
        sampler.minFilter == GL_NEAREST_MIPMAP_LINEAR ||
        sampler.minFilter == GL_LINEAR_MIPMAP_NEAREST ||
        sampler.minFilter == GL_LINEAR_MIPMAP_LINEAR) {
      glGenerateMipmap(GL_TEXTURE_2D);
    }
  }
  glBindTexture(GL_TEXTURE_2D, 0);

  return textureObjects;
}